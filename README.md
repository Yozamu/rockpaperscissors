Rock paper scissors (Java, console mode)
-
Open the project in your IDE and run the program, or follow these instructions:  

- Open a terminal and *cd* to where the project is located  
- Compile with *javac **  
- Run the program with *java Main*  

The program can easily be extended by adding new choices in the Choice.java enum.