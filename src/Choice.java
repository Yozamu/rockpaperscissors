public enum Choice {
    ROCK("Rock", new String[] { "Scissors" }, new String[] { "Paper" }),
    PAPER("Paper", new String[] { "Rock" }, new String[] { "Scissors" }),
    SCISSORS("Scissors", new String[] { "Paper" }, new String[] { "Rock" });

    private String name;
    private String[] winsOver;
    private String[] losesTo;

    Choice(String name, String[] winsOver, String[] losesTo) {
        this.name = name;
        this.winsOver = winsOver;
        this.losesTo = losesTo;
    }

    public String getName() {
        return name;
    }

    public String[] getWinsOver() {
        return winsOver;
    }

    public String[] getLosesTo() {
        return losesTo;
    }
}
