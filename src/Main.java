import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        GameManager game = new GameManager(
                new Player("Player1", true),
                new Player("Player2", false)
        );
        String command = "";
        Scanner sc = new Scanner(System.in);

        while (!"exit".equals(command)) {
            game.displayWaitingMessage();
            command = sc.nextLine();
            game.executeCommand(command);
        }
    }
}
