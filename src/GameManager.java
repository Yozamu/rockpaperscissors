import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GameManager {
    private Player player1;
    private Player player2;
    private int round;

    private Choice[] choices;
    private Random random;

    public GameManager(Player p1, Player p2) {
        player1 = p1;
        player2 = p2;
        round = 1;
        choices = Choice.values();
        random = new Random();
    }

    public void displayWaitingMessage() {
        System.out.println("");
        System.out.println(player1.getPlayerInformation());
        System.out.println(player2.getPlayerInformation());
        System.out.println("==========");
        System.out.println("Next round is ready. Type 'play' to start round " + round);
        System.out.println("(Available commands are: play, computer, human, exit)");
    }

    public void executeCommand(String command) {
        Command cmd;
        try {
            cmd = Command.valueOf(command.toUpperCase());
            switch (cmd) {
                case PLAY:
                    playRound();
                    break;
                case COMPUTER:
                    changePlayer1control(false);
                    break;
                case HUMAN:
                    changePlayer1control(true);
                    break;
                case EXIT:
                    break;
                default:
                    System.out.println("Unknown command");
            }
        } catch (IllegalArgumentException error) {
            System.out.println("Unknown command");
        }
    }

    private void playRound() {
        round++;
        if (player1.isHuman()) {
            player1.setChoice(choices[askPlayerChoice()]);
        } else {
            player1.setChoice(choices[random.nextInt(choices.length)]);
        }
        player2.setChoice(choices[random.nextInt(choices.length)]);
        displayWinner();
    }

    private void displayWinner() {
        List<String> p1winsOver = Arrays.asList(player1.getChoice().getWinsOver());
        List<String> p1losesTo = Arrays.asList(player1.getChoice().getLosesTo());
        System.out.println(player1.getPlayedInformation() + ", " + player2.getPlayedInformation());
        if (p1winsOver.contains(player2.getChoice().getName())) {
            player1.setScore(player1.getScore() + 1);
            System.out.println(player1.getName() + " wins this round !");
        } else if (p1losesTo.contains(player2.getChoice().getName())) {
            player2.setScore(player2.getScore() + 1);
            System.out.println(player2.getName() + " wins this round !");
        } else {
            System.out.println("Draw !");
        }
    }

    private int askPlayerChoice() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type the number corresponding to your choice :");
        for (int i = 0; i < choices.length; i++) {
            System.out.println(i + ". " + choices[i].getName());
        }
        return sc.nextInt();
    }

    private void changePlayer1control(boolean humanControlled) {
        player1.setHuman(humanControlled);
        System.out.println("Player 1 is now a " + (humanControlled ? "human" : "computer"));
    }

    public Player getPlayer1() {
        return player1;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }
}
