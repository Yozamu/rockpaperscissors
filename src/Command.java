public enum Command {
    PLAY("play"),
    HUMAN("play"),
    COMPUTER("play"),
    EXIT("play");

    private String name;

    Command(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
